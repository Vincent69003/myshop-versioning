- <Vincent MARQUIS> <Ilyess RAMDANI>
- Etapes réalisées dans le projet : 
Création du contexte de travail : chaque binôme chacun de son côté
Binôme 1 : mise en oeuvre des consignes du 3.5
Binôme 2 : mise en oeuvre des consignes du 3.6 et 3.7
Puis :
Binôme 1 : mise en oeuvre featureA
Binôme 2 : mise en oeuvre featureB
(répartition des taches dans le binome)

PS : nous avons rencontré des difficultés :
- au moment de la résolution des conflits. (perte de tout le travail du binôme 2 sur featureB).
- apparition d'un fichier caché .DS_store... posant systématiquement problème.
- problème de connexion découverte tardivement qui posait problème de résolution (forcément!)

Tâches non réalisées dans le projet par manque de temps : fonctionnalités du point 6 (même si nous avons compris qu'il fallait mettre des getters et setters), test unitaires.