package com.ci.myShop.model;

public class Item {
	String name;
	int id;
	float price;
	int nbrElt;
	
	
	
	
	public String getName() {
		return name;
	}
	public int getId() {
		return id;
	}
	public float getPrice() {
		return price;
	}
	public int getNbrElt() {
		return nbrElt;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}
	
	public String display() {
		//: permettant de mettre en forme l�objet dans une String
		System.out.println("Nom produit : " + name + " - ID : " + id + " - Prix: " + price + " euros - Quantit� : " + nbrElt);
		return String.format("Nom produit : " + name + " - ID : " + id + " - Prix: " + price + " euros - Quantit� : " + nbrElt);
	}
}
	
